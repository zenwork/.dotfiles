#!/usr/bin/env bash

echo '### Create new venv python3'
python3 -m venv venv
echo '### Activate venv'
source venv/bin/activate
echo '### Upgrade pip'
pip install --upgrade pip
echo '###Upgrade setuptools'
pip install --upgrade setuptools
echo '### Install pip-tools'
pip install pip-tools
echo '### Use pip-sync for install requirements'

#!/usr/bin/env bash

xclip -o -selection clipboard | ~/.dotfiles/bin/trans -b -no-auto

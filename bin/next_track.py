#!/usr/bin/env python3
import dbus
import os
import time


stream = os.popen("ps aux | grep -i firefox | grep -v grep | grep -v contentproc | awk {'print $2'}")
ff_pid = stream.read().strip()
ff_bus_name = f'org.mpris.MediaPlayer2.firefox.instance{ff_pid}'
bus_path = '/org/mpris/MediaPlayer2'
bus_obj = 'org.mpris.MediaPlayer2.Player'

bus = dbus.SessionBus()
proxy = bus.get_object(ff_bus_name, bus_path)

player = dbus.Interface(proxy, dbus_interface=bus_obj)
player.Next()

time.sleep(2)

interface = dbus.Interface(proxy, dbus_interface="org.freedesktop.DBus.Properties")
properties = interface.GetAll(bus_obj)
metadata = properties["Metadata"]
artist = str(metadata["xesam:artist"][0])
title = str(metadata["xesam:title"])
os.system(f"notify-send -t 3000 '{artist}\n{title}'")

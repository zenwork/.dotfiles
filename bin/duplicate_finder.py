#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess

all_files = []
for directory, subdir, files in os.walk('.'):
    for f in files:
        f = os.path.abspath(os.path.join(directory, f))
        all_files.append(f)

total_files = len(all_files)

print('Total files: %s. Calc md5 hash...' % total_files)

all_files_hash = []
for i, f in enumerate(all_files):
    cmd = 'md5sum "%s"' % f
    md5 = subprocess.getoutput(cmd)
    all_files_hash.append(md5)
    print('[%s/%s] %s' % (i, total_files, md5))

all_files_hash.sort()

all_files = []
for i in all_files_hash:
    f = [
        i[:32],
        i[34:]
    ]
    all_files.append(f)

duplicates = {}
prev_f = False
for f in all_files:
    if not prev_f:
        prev_f = f
        continue
    if prev_f[0] == f[0]:
        if f[0] not in duplicates:
            duplicates[f[0]] = [prev_f[1]]
        duplicates[f[0]].append(f[1])
    prev_f = f

duplicates_list = list(duplicates.values())
duplicates_list.sort(key=lambda x: x[0])

for duplicat in duplicates_list:
    print('-' * 79)
    for d in duplicat:
        print(d)

print('-' * 79)
print('Summary: %s duplicate group' % len(duplicates_list))

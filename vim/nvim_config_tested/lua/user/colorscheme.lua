vim.cmd [[
try
  colorscheme PaperColor
  set background=dark
  set fillchars+=vert:\ 
  hi VertSplit cterm=NONE ctermfg=NONE ctermbg=234
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
  set background=dark
endtry
]]

" source ~/.config/nvim/plug.vim

" Base settings ---------------------------------------------------

set background=dark
colorscheme PaperColor

" filetype off
set clipboard=unnamedplus   " Set global buffer
set splitright
set enc=utf-8
set ls=2
set colorcolumn=""
set foldlevel=0
set hidden
set tabstop=4
set shiftwidth=4
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
set smarttab
set expandtab
set smartindent
set number
set relativenumber
set incsearch
set ignorecase
set cursorline
set nobackup
set nowb
set noswapfile
set splitbelow  " Doc window bottom
set updatetime=500 " for GitGutter
set fillchars+=vert:\ 
hi VertSplit cterm=NONE ctermfg=NONE ctermbg=234


set wildmode=longest:full,full  " Bash-like completion

" Base maps ---------------------------------------------------------
let mapleader = " "

nnoremap   <SPACE>   <Nop>
inoremap   jj        <Esc>
nnoremap   <tab>     <c-w>w
nnoremap   <S-tab>   <c-w>W
map        Q         :bdelete<CR>
" map        q         :bdelete<CR>

" Vim external buffer 
" vmap <Leader>y :w! ~/.vbuf<CR>
" nmap <Leader>y :.w! ~/.vbuf<CR>
" nmap <Leader>p :r ~/.vbuf<CR>

" Windows nav
map <C-K> <C-w><Up>
map <C-J> <C-w><Down>
map <C-L> <C-w><Right>
map <C-H> <C-w><Left>

" --------------------------------------------------------------------
" Plugins conf 
" --------------------------------------------------------------------

" " kien/rainbow_parentheses.vim --------------------------------------
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces


" w0rp/ale ----------------------------------------------------------
nmap <silent> [e <Plug>(ale_previous_wrap)
nmap <silent> ]e <Plug>(ale_next_wrap)
let g:ale_sign_error = 'E'
let g:ale_sign_warning = 'W'
let g:ale_python_pylint_options='--disable C0111,C0103,R0903,E1101,E1129,R0201,R0901,W0212,W0201,R0914,W0511,W0622,W0621,R1707'
" let g:ale_linters = {
" \   'python': ['flake8', 'pylint'] 
" \}

" \   'python': ['isort', 'yapf'],
let g:ale_fixers = {
\   'python': ['yapf'],
\}
let g:ale_fix_on_save = 1


" SirVer/ultisnips --------------------------------------------------
" let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetsDir="~/.vim/snippets"
" autocmd FileType python UltiSnipsAddFiletypes django
" autocmd FileType html UltiSnipsAddFiletypes htmldjango


" jpalardy/vim-slime ------------------------------------------------
" let g:slime_target           = "tmux"
" let g:slime_default_config   = {"socket_name": "default", "target_pane": "2"}
" let g:slime_dont_ask_default = 1
" let g:slime_no_mappings      = 1
" let g:slime_python_ipython   = 1
" xmap <leader>s <Plug>SlimeRegionSend
" nmap <leader>s <Plug>SlimeLineSend


" stevearc/vim-arduino ----------------------------------------------
" let g:arduino_serial_cmd = 'picocom {port} -b {baud} -l'
" let g:arduino_serial_port = '/dev/ttyACM0'
" let g:arduino_serial_baud = 9600
" let g:arduino_board = 'arduino:avr:mega'
" " nnoremap <buffer> <leader>au :ArduinoUpload<CR>
" nnoremap <silent> <F9> :ArduinoUpload<CR>


" easymotion/vim-easymotion -----------------------------------------
map <Nop> <Plug>(easymotion-prefix)
map <silent> f <Plug>(easymotion-s2)
vmap <silent> f <Plug>(easymotion-s2)
map  FN <Plug>(easymotion-next)
map  FP <Plug>(easymotion-prev)
" map <silent> f <Plug>(easymotion-f)
" map <silent> F <Plug>(easymotion-F)
let g:EasyMotion_smartcase = 1


" Pklen/python-mode -------------------------------------------------
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_rename_bind = '<leader>R'
let g:pymode_doc = 0
let g:pymode_doc_key = 'K'
let g:pymode_lint = 0
let g:pymode_lint_checkers = ['pylint', 'pyflakes', 'pep8', 'mccabe']
let g:pymode_lint_ignore="C0111,E1129,R0201,W0212,W0201,R0914,W0511"
let g:pymode_lint_write = 0
let g:pymode_virtualenv = 1
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<leader>d'
let g:pymode_breakpoint_cmd = '__import__("pudb").set_trace()'
let g:pymode_syntax = 0
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_folding = 0
let g:pymode_run = 1
let g:pymode_run_bind = '<F8>'
let g:jedi#popup_on_dot = 0
let g:jedi#smart_auto_mappings = 1
let g:jedi#goto_command = 'gd'
let g:jedi#goto_assignments_command = ''

" inoremap <C-space> <C-x><C-o>
autocmd FileType python set colorcolumn=0
autocmd FileType python map + ys$)i


" tomtom/tcomment_vim -----------------------------------------------
map - gcc
vmap - gc


" scrooloose/nerdtree -----------------------------------------------
map <F3> :NERDTreeToggle<CR>
map <leader>f :NERDTreeFind<CR>
let NERDTreeQuitOnOpen=1
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$', '__pycache__$',]


" bling/vim-airline -------------------------------------------------
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols_ascii = 1
let g:airline_symbols.colnr = '::'
let g:airline_symbols.linenr = ' '
let g:airline_symbols.maxlinenr = ''
let g:airline_theme='bubblegum'


" mattn/emmet-vim ---------------------------------------------------
let g:user_emmet_install_global = 0
autocmd FileType html,css,less,htmldjango EmmetInstall
" let g:user_emmet_expandabbr_key = '<C-space>'


" junegunn/fzf.vim --------------------------------------------------
map <leader>b :Buffers <CR>
map <leader><leader> :GFiles <CR>
map <leader>/ :Ag <C-r><C-w><CR>
nnoremap <silent> <bs> :Ag <C-r><C-w><CR>
" nnoremap <C-f> :Ag<space>
" TODO add bs in visual mode
map <leader>; :History: <CR>
map <leader>h :History <CR>
map <leader>l :Lines <CR>
" map <leader>g :Git<CR>
map <leader>gs :Git<CR>
map <leader>gf :diffget //2<CR>
map <leader>gj :diffget //3<CR>

map <leader>s :Snippets <CR>
map <leader>c :Commands<CR>
inoremap <C-r> <ESC>:Snippets <CR>

" let g:fzf_layout = { 'down': '~80%' }
" command! -bang -nargs=* Ag
"   \ call fzf#vim#ag(<q-args>, fzf#vim#with_preview('right'), <bang>0)
" command! -bang -nargs=* GFiles
"   \ call fzf#vim#gitfiles(<q-args>, fzf#vim#with_preview('right'), <bang>0)
"
" command! -nargs=+ -complete=dir AgDir call fzf#vim#ag_raw(<q-args>, fzf#vim#with_preview('right'), <bang>0)


" majutsushi/tagbar -------------------------------------------------
nnoremap <leader>t :TagbarOpenAutoClose <CR>
let g:tagbar_foldlevel = 0


" vim-bookmarks -----------------------------------------------------
let g:bookmark_auto_close = 1
let g:bookmark_center = 1


" vimwiki/vimwiki ---------------------------------------------------
let g:vimwiki_list = [{'path': '~/info/wiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" *.md file assoc from markdown syntax
autocmd BufNewFile,BufRead *.md set filetype=markdown


" diepm/vim-rest-console --------------------------------------------
let g:vrc_split_request_body = 1
let g:vrc_auto_format_uhex = 1
" let g:vrc_set_default_mapping = 0
nnoremap <silent> <F9> <C-w>o:call VrcQuery()<CR>
" autocmd FileType rest map <C-j> <C-w>o:call VrcQuery()<CR>
 

" Toggle paste mode
nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>


nnoremap <silent> <F12> :w<CR>:make less-2-css<CR><CR><CR>
inoremap <silent> <F12> <ESC>:w<CR>:make less-2-css<CR><CR><CR>
" autocmd bufwritepost *.less :make less-2-css | <CR><CR>

map <C-j> :bnext<CR>
map <C-k> :bprevious<CR>


" -------------------------------------------------------------------
" source ~/.config/nvim/langmap.vim
" source ~/.config/nvim/coc.vim
" source ~/.config/nvim/lsp_conf.vim
" source ~/.config/nvim/lsp.lua
" source ~/.config/nvim/cmp.lua
" source ~/.config/nvim/other.vim

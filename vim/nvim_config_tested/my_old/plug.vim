" Plug --------------------------------------------------------------

call plug#begin('~/.vim/plugged')

Plug 'NLKNguyen/papercolor-theme'

" Base 
Plug 'scrooloose/nerdtree'
Plug 'tomtom/tcomment_vim'

Plug 'bling/vim-airline' 
Plug 'vim-airline/vim-airline-themes'
Plug 'majutsushi/tagbar'
Plug 'mileszs/ack.vim'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'easymotion/vim-easymotion'
Plug 'jiangmiao/auto-pairs'
Plug 'kien/rainbow_parentheses.vim'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'

" COC
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'

" CMP
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" Markdown
Plug 'asford/tagbar-markdown.vim'
" Plug 'masukomi/vim-markdown-folding'

" AutoComplite
" Plug 'Shougo/deoplete.nvim'
" Plug 'roxma/nvim-yarp'
" Plug 'roxma/vim-hug-neovim-rpc'
" Plug 'zchee/deoplete-jedi'

" Html
" Plug 'groenewege/vim-less'
" Plug 'mattn/emmet-vim'
" Plug 'gregsexton/MatchTag'
" Plug 'mtscout6/vim-tagbar-css'

" " Python
" Plug 'klen/python-mode'	
" Plug 'davidhalter/jedi-vim'
" " Plug 'mitsuhiko/vim-jinja'
" " Plug 'mitsuhiko/vim-python-combined' ?
" Plug 'bps/vim-textobj-python'   
" Plug 'kana/vim-textobj-user'   
" " Plug 'michaeljsmith/vim-indent-object' 
" " Plug 'mjbrownie/vim-htmldjango_omnicomplete'
" Plug 'tweekmonster/django-plus.vim'


" Fzf
Plug 'junegunn/fzf' ", { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'gfanto/fzf-lsp.nvim'
Plug 'sharkdp/bat'

" GIT
Plug 'tpope/vim-fugitive'
Plug 'tommcdo/vim-fugitive-blame-ext'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/gv.vim'

" " Snippets
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'

" wiki
Plug 'vimwiki/vimwiki'

" TMUX - send command to split window
Plug 'christoomey/vim-tmux-runner'

" REST
Plug 'diepm/vim-rest-console'

" Maybe... ---------------------------------------------------------

" Go
" Plug 'fatih/vim-go'

" Arduino
" Plug 'stevearc/vim-arduino'

" Other
" Plug 'liuchengxu/vim-which-key'
" Plug 'morhetz/gruvbox'
" Plug 'christoomey/vim-tmux-navigator'
" Plug 'jpalardy/vim-slime'
" Plug 'brooth/far.vim'
" Plug 'junegunn/goyo.vim'
" Plug 'w0rp/ale'
" Plug 'eugen0329/vim-esearch'

call plug#end()

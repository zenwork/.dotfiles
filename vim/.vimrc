set nocompatible

" Theme -------------------------------------------------------------
" let g:molokai_original = 1
" let g:rehash256 = 1
" colorscheme molokai

" Base conf ---------------------------------------------------------
filetype off
set clipboard=unnamedplus   " Set global buffer
set splitright
set enc=utf-8
set ls=2
set colorcolumn=""
set foldlevel=0
set hidden
set tabstop=4
set shiftwidth=4
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
set smarttab
set expandtab
set smartindent
set number
set relativenumber
set incsearch
set ignorecase
set cursorline
set nobackup
set nowb
set noswapfile
set splitbelow  " Doc window bottom
set updatetime=500 " for GitGutter
set fillchars+=vert:\ 
hi VertSplit cterm=NONE ctermfg=NONE ctermbg=235

set wildmenu 
set wildmode=longest:full,full  " Bash-like completion

set langmap=ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>
nmap Ж :
" yank
nmap Н Y
nmap з p
nmap ф a
nmap щ o
nmap г u
nmap З P

" Base maps ---------------------------------------------------------
let mapleader = " "

nnoremap   <SPACE>   <Nop>
inoremap   jj        <Esc>
nnoremap   <tab>     <c-w>w
nnoremap   <S-tab>   <c-w>W
map        Q         :bdelete<CR>
" map        q         :bdelete<CR>

" Vim external buffer 
vmap <Leader>y :w! ~/.vbuf<CR>
nmap <Leader>y :.w! ~/.vbuf<CR>
nmap <Leader>p :r ~/.vbuf<CR>

" Windows nav
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>
map <C-w>z :ZoomToggle<CR>
map <C-w>t :TabooOpen 

map H :tabprevious<CR>
map L :tabnext<CR>


" Set locale US to base mode ----------------------------------------
" function! SetUsLayout()
"       silent !setxkbmap us  
"       silent !setxkbmap us,ru  
" endfunction
" autocmd InsertLeave * call SetUsLayout()

" Plug --------------------------------------------------------------
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'bling/vim-airline' 
Plug 'vim-airline/vim-airline-themes'
Plug 'majutsushi/tagbar'
Plug 'mileszs/ack.vim'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'gcmt/taboo.vim'
" Plug 'christoomey/vim-tmux-navigator'
Plug 'easymotion/vim-easymotion'
" Plug 'phaazon/hop.nvim'
Plug 'jiangmiao/auto-pairs'
" Plug 'kien/rainbow_parentheses.vim'
" Plug 'jpalardy/vim-slime'
Plug 'tpope/vim-surround'
Plug 'tomtom/tcomment_vim'
" Plug 'janko-m/vim-test'
Plug 'godlygeek/tabular'
" Plug 'brooth/far.vim'
Plug 'junegunn/goyo.vim'
Plug 'w0rp/ale'
" Plug 'dhruvasagar/vim-zoom'
Plug 'markstory/vim-zoomwin'

" Plug 'eugen0329/vim-esearch'

" markdown
Plug 'asford/tagbar-markdown.vim'
" Plug 'masukomi/vim-markdown-folding'


Plug 'NLKNguyen/papercolor-theme'


" autocomplite
" Plug 'Shougo/deoplete.nvim'
" Plug 'roxma/nvim-yarp'
" Plug 'roxma/vim-hug-neovim-rpc'
Plug 'zchee/deoplete-jedi'

" Html
" Plug 'groenewege/vim-less'
" Plug 'mattn/emmet-vim'
" Plug 'gregsexton/MatchTag'
" Plug 'mtscout6/vim-tagbar-css'

" Python
Plug 'klen/python-mode'	
Plug 'davidhalter/jedi-vim'
" Plug 'mitsuhiko/vim-jinja'
" Plug 'mitsuhiko/vim-python-combined' ?
Plug 'bps/vim-textobj-python'   
Plug 'kana/vim-textobj-user'   
" Plug 'michaeljsmith/vim-indent-object' 
" Plug 'mjbrownie/vim-htmldjango_omnicomplete'
Plug 'tweekmonster/django-plus.vim'

" Go
" Plug 'fatih/vim-go'

" Fzf
Plug 'junegunn/fzf' ", { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" GIT
Plug 'tpope/vim-fugitive'
Plug 'tommcdo/vim-fugitive-blame-ext'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/gv.vim'
" Plug 'tommcdo/vim-fubitive'
" Plug 'int3/vim-extradite'

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Arduino
" Plug 'stevearc/vim-arduino'

" wiki
Plug 'vimwiki/vimwiki'

" TMUX
" Plug 'christoomey/vim-tmux-runner'

" REST
Plug 'diepm/vim-rest-console'
" Plug 'xavierchow/vim-swagger-preview'

" Tested
" Plug 'liuchengxu/vim-which-key'

call plug#end()

" Shougo/deoplete.nvim ---------------------------------------------
" let g:deoplete#enable_at_startup=1

" Color Scheme -----------------------------------------------------

let g:PaperColor_Theme_Options = {
\   'theme': {
\     'default.dark': {
\       'override' : {
\       'tabline_active_fg':   ['', '235'],
\       'tabline_active_bg':   ['', '172'],
\       'tabline_inactive_fg': ['', '172'],
\       'tabline_inactive_bg': ['#262626', '235'],
\       }
\     }
\   }
\ }

set background=dark
colorscheme PaperColor


" " kien/rainbow_parentheses.vim --------------------------------------
" au VimEnter * RainbowParenthesesToggle
" au Syntax * RainbowParenthesesLoadRound
" au Syntax * RainbowParenthesesLoadSquare
" au Syntax * RainbowParenthesesLoadBraces

" w0rp/ale ----------------------------------------------------------
nmap <silent> [e <Plug>(ale_previous_wrap)
nmap <silent> ]e <Plug>(ale_next_wrap)
let g:ale_sign_error = 'E'
let g:ale_sign_warning = 'W'
let g:ale_python_pylint_options='--disable C0111,C0103,R0903,E1101,E1129,R0201,R0901,W0212,W0201,R0914,W0511,W0622,W0621,R1707'
" let g:ale_linters = {
" \   'python': ['flake8', 'pylint'] 
" \}

" \   'python': ['isort', 'yapf'],
let g:ale_fixers = {
\   'python': ['yapf'],
\}
let g:ale_fix_on_save = 1

" SirVer/ultisnips --------------------------------------------------
" let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetsDir="~/.vim/snippets"
autocmd FileType python UltiSnipsAddFiletypes django
autocmd FileType html UltiSnipsAddFiletypes htmldjango


" jpalardy/vim-slime ------------------------------------------------
" let g:slime_target           = "tmux"
" let g:slime_default_config   = {"socket_name": "default", "target_pane": "2"}
" let g:slime_dont_ask_default = 1
" let g:slime_no_mappings      = 1
" let g:slime_python_ipython   = 1
" xmap <leader>s <Plug>SlimeRegionSend
" nmap <leader>s <Plug>SlimeLineSend

" stevearc/vim-arduino ----------------------------------------------
let g:arduino_serial_cmd = 'picocom {port} -b {baud} -l'
let g:arduino_serial_port = '/dev/ttyACM0'
let g:arduino_serial_baud = 9600
let g:arduino_board = 'arduino:avr:mega'
" nnoremap <buffer> <leader>au :ArduinoUpload<CR>
nnoremap <silent> <F9> :ArduinoUpload<CR>

" easymotion/vim-easymotion -----------------------------------------
map <Nop> <Plug>(easymotion-prefix)
map <silent> f <Plug>(easymotion-s2)
vmap <silent> f <Plug>(easymotion-s2)
" map <silent> f <Plug>(easymotion-jumptoanywhere)

" map  FN <Plug>(easymotion-next)
" map  FP <Plug>(easymotion-prev)
" map <silent> f <Plug>(easymotion-f)
" map <silent> F <Plug>(easymotion-F)
let g:EasyMotion_smartcase = 1

" Pklen/python-mode -------------------------------------------------
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_rename_bind = '<leader>R'
let g:pymode_doc = 0
let g:pymode_doc_key = 'K'
let g:pymode_lint = 0
let g:pymode_lint_checkers = ['pylint', 'pyflakes', 'pep8', 'mccabe']
let g:pymode_lint_ignore="C0111,E1129,R0201,W0212,W0201,R0914,W0511"
let g:pymode_lint_write = 0
let g:pymode_virtualenv = 1
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<leader>d'
let g:pymode_breakpoint_cmd = '__import__("pudb").set_trace()'
let g:pymode_syntax = 0
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_folding = 0
let g:pymode_run = 1
let g:pymode_run_bind = '<F8>'
let g:jedi#popup_on_dot = 0
let g:jedi#smart_auto_mappings = 1
let g:jedi#goto_command = 'gd'
let g:jedi#goto_assignments_command = ''

" inoremap <C-space> <C-x><C-o>
autocmd FileType python set colorcolumn=0
autocmd FileType python map + ys$)i

" tomtom/tcomment_vim -----------------------------------------------
map - gcc
vmap - gc

" scrooloose/nerdtree -----------------------------------------------
map <F3> :NERDTreeToggle<CR>
map <leader>f :NERDTreeFind<CR>
let NERDTreeQuitOnOpen=1
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$', '__pycache__$',]

" bling/vim-airline -------------------------------------------------
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols_ascii = 1
let g:airline_symbols.colnr = '::'
let g:airline_symbols.linenr = ' '
let g:airline_symbols.maxlinenr = ''
let g:airline_theme='bubblegum'

" mattn/emmet-vim ---------------------------------------------------
let g:user_emmet_install_global = 0
autocmd FileType html,css,less,htmldjango EmmetInstall
" let g:user_emmet_expandabbr_key = '<C-space>'

" junegunn/fzf.vim --------------------------------------------------
map <leader>b :Buffers <CR>
map <leader><leader> :GFiles <CR>

map <leader>/ :Ag <C-r><C-w><CR>
nnoremap <silent> <bs> :Ag <C-r><C-w><CR>
" nnoremap <C-f> :Ag<space>

" TODO add bs in visual mode

map <leader>; :History: <CR>
map <leader>h :History <CR>
map <leader>l :Lines <CR>

" map <leader>g :Git<CR>
map <leader>gs :Git<CR>
map <leader>gf :diffget //2<CR>
map <leader>gj :diffget //3<CR>

map <leader>s :Snippets <CR>
map <leader>c :Commands<CR>
inoremap <C-r> <ESC>:Snippets <CR>

let g:fzf_layout = { 'down': '~80%' }
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>, fzf#vim#with_preview('right'), <bang>0)
command! -bang -nargs=* GFiles
  \ call fzf#vim#gitfiles(<q-args>, fzf#vim#with_preview('right'), <bang>0)

command! -nargs=+ -complete=dir AgDir call fzf#vim#ag_raw(<q-args>, fzf#vim#with_preview('right'), <bang>0)

" majutsushi/tagbar -------------------------------------------------
nnoremap <leader>t :TagbarOpenAutoClose <CR>
let g:tagbar_foldlevel = 0

" vim-bookmarks -----------------------------------------------------
let g:bookmark_auto_close = 1
let g:bookmark_center = 1

" vimwiki/vimwiki ---------------------------------------------------
let g:vimwiki_list = [{'path': '~/info/wiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" *.md file assoc from markdown syntax
autocmd BufNewFile,BufRead *.md set filetype=markdown

" diepm/vim-rest-console --------------------------------------------
let g:vrc_split_request_body = 1
let g:vrc_auto_format_uhex = 1
" let g:vrc_set_default_mapping = 0
nnoremap <silent> <F9> <C-w>o:call VrcQuery()<CR>
" autocmd FileType rest map <C-j> <C-w>o:call VrcQuery()<CR>
 
" Source the vimrc file after saving it -----------------------------
if has("autocmd")
  autocmd bufwritepost .vimrc source $MYVIMRC
endif

" Encoding menu -----------------------------------------------------
set wcm=<Tab>
menu Encoding.koi8-r :e ++enc=koi8-r<CR>
menu Encoding.windows-1251 :e ++enc=cp1251<CR>
menu Encoding.cp866 :e ++enc=cp866<CR>
menu Encoding.utf-8 :e ++enc=utf8 <CR>
map <F8> :emenu Encoding.<TAB>

" Toggle paste mode
nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>

" " Translate buffer
" nnoremap <silent> <leader>gt :call BufTranslate()<CR><CR>
" function! BufTranslate()
"     exe "normal ggyG"
"     vnew /tmp/vimtr
"     set paste
"     put
"     normal jdGggdd
"     exe "%!trans -b -no-auto"
"     set nopaste
"     write
"     nnoremap <buffer> q :q<CR>
" endfunction 

nnoremap <silent> <F12> :w<CR>:make less-2-css<CR><CR><CR>
inoremap <silent> <F12> <ESC>:w<CR>:make less-2-css<CR><CR><CR>
" autocmd bufwritepost *.less :make less-2-css | <CR><CR>

" Highlight word under cursor ---------------------------------------
" http://vim.wikia.com/wiki/Auto_highlight_current_word_when_idle
" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
nnoremap U :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>
function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    echo 'Highlight current word: off'
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    echo 'Highlight current word: ON'
    return 1
  endif
endfunction

" f &term =~ "xterm\\|rxvt"
"     " use an orange cursor in insert mode
"     let &t_SI = "\<Esc>]12;orange\x7"
"     " use a red cursor otherwise
"     let &t_EI = "\<Esc>]12;red\x7"
"     silent !echo -ne "\033]12;red\007"
"     " reset cursor when vim exits
"     autocmd VimLeave * silent !echo -ne "\033]112\007"
"     " use \003]12;gray\007 for gnome-terminal
" endif
" 
" map <C-j> :bnext<CR>
" map <C-k> :bprevious<CR>

" run current script with python3 by CTRL+R in command and insert mode
autocmd FileType python map <buffer> <C-r> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <C-r> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>


#!/bin/bash

# From https://github.com/masksshow/Thinkpad-E14-15-AMD-Gen-2-FIX

sudo apt-get update
sudo apt-get install acpidump
sudo ./thinkpad-e15-gen2-firmware-fix.sh  --grub-setup-dsdt

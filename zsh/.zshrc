export PATH=$HOME/.dotfiles/bin:$HOME/go/bin:$PATH
export ZSH=~/.oh-my-zsh

# ZSH_THEME="fishy"
ZSH_THEME="maran"
DISABLE_AUTO_TITLE="true"
DISABLE_AUTO_UPDATE="true"

plugins=(git fabric fancy-ctrl-z pip vagrant tmux docker docker-compose)

source $ZSH/oh-my-zsh.sh

alias zsh-set-default='sudo usermod -s $(which zsh) $(whoami)'

#################################################

export EDITOR=nvim

export DISABLE_AUTO_TITLE=true

alias pcsleep='systemctl suspend'
alias swapclean='sudo swapoff -a && sudo swapon -a'
alias nm-applet-reset="killall nm-applet; nohup nm-applet &"
alias pc-info='inxi -F'
alias tmux-kill-current-session='tmux kill-session -t `tmux display-message -p "#S"`'
alias tmux-new="tmux new-session -s"
alias e='nvim'
alias e-not-commit='nvim $(git ls-files -o -m --exclude-standard)'
alias ww='nvim -c "VimwikiIndex"'
alias mc='mc -b -d'
alias m='make'
alias c='clear'
alias L='less'
alias du='du -h'
alias diff="grc diff -u"
alias cal='ncal'
alias vg='vagrant'
function du-summary() { du $1 --max-depth=1 --time | sort -h -r  }
alias count-files-in-dir='find . -type f | wc -l'
alias foresterSync='rsync --progress --delete -av /home/f/ /media/f/Backup/'
alias pdf="zathura --fork --mode=fullscreen"
# xfce4-terminal -e 'zsh -c "xclip -o | qrcode-terminal; echo; xclip -o; zsh"' -T "QR code" --fullscreen
alias q="xclip -o | qrcode-terminal"
# Docker
alias dc="docker-compose"
alias dcu="docker-compose up --build"
alias docker-kill-all='docker kill $(docker ps -q)'
alias docker-rm-all='docker rm $(docker ps -a -q)'
docker-removecontainers() {
    docker stop $(docker ps -aq)
    docker rm $(docker ps -aq)
}

docker-clear-ALL() {
    docker-removecontainers
    docker network prune -f
    docker rmi -f $(docker images --filter dangling=true -qa)
    docker volume rm $(docker volume ls --filter dangling=true -q)
    docker rmi -f $(docker images -qa)
}

alias tr-en2ru="trans en:ru"
alias tr-ru2en="trans ru:en"
alias en="trans ru:en"
alias ru="trans en:ru"

# Go
alias golang=$(which go)

# Git
alias gs="git status -s -u"
alias glg="git lg2"
alias glg-more="git lg2 --stat"
alias go="git checkout"

# Python
alias py-clean='find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs sudo rm -rf'
# alias pv="source venv/bin/activate 2> /dev/null || source ../venv/bin/activate"
# alias py-venv-create="py-venv-create.sh && pv"

alias pt='py.test'
alias pv='source ./.venv/bin/activate'

fpath=(~/.dotfiles/zsh/completions $fpath)
# autoload -U compinit
# compinit
# zstyle ':completion:*' menu select=2

export PIP_FORMAT=columns
export PYTHONSTARTUP="$HOME/.pythonrc.py"
export PIPENV_IGNORE_VIRTUALENVS=1
alias pipenv-completion-enable='eval "$(pipenv --completion)"'
alias pe='pipenv'
alias f='fab'
alias fd='fab deploy'
alias r='./manage.py runserver'
# Fabfile autocomplite #######################################################
# _complete_fab() {
#     collection_arg=''
#     if [[ "${words}" =~ "(-c|--collection) [^ ]+" ]]; then
#         collection_arg=$MATCH
#     fi
#     reply=( $(fab ${=collection_arg} --complete -- ${words}) )
# }
# compctl -K _complete_fab + -f fab
##############################################################################

export GOPATH='/home/f/go/'

autoload bashcompinit
bashcompinit
. django_bash_completion    

# end python

# Taskwarrior
# alias t="task"
# alias in="task add +in"

# pulseaudio
# alias pulseaudio-reload='/usr/bin/pasuspender /bin/true'
alias pulseaudio-reload='systemctl --user restart pulseaudio.service'
alias pa-reload='systemctl --user restart pulseaudio.service'


# wifi
alias wifi-reload='sudo service network-manager restart'

# Autoreload commands
zstyle ':completion:*' rehash true

case "$TERM" in
    'xterm') TERM=xterm-256color;;
    'screen') TERM=screen-256color;;
    'Eterm') TERM=Eterm-256color;;
esac

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='ag -g ""'

mcd(){ mkdir -p "$*"; cd "$*" }

# Run after cd 
function chpwd(){ 
    l;
    if [ -f ./info.txt ]; then
        echo ''
        echo '### Info ###'
        cat ./info.txt;
        echo ''
    fi
}

o () {
    if [ -f $1 ] ; then
        case $1 in
            *.pdf)  pdf $1 ;;
            *.djvu) pdf $1 ;;
            *.fb2.zip) fbreader $1 ;;
            *.fb2) fbreader $1 ;;
            *.html) firefox $1 ;;
            *) thunar $1;;
            # *)      echo "я не в курсе как открыть '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# распаковка архива
extract () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1        ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1       ;;
            *.rar)       unrar x $1     ;;
            *.gz)        gunzip $1     ;;
            *.tar)       tar xf $1        ;;
            *.tbz2)      tar xjf $1      ;;
            *.tgz)       tar xzf $1       ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1    ;;
            *)           echo "я не в курсе как распаковать '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}


# упаковка в архив
pk () {
    if [ $1 ] ; then
        case $1 in
            tbz)   	tar cjvf $2.tar.bz2 $2      ;;
            tgz)   	tar czvf $2.tar.gz  $2   	;;
            tar)  	tar cpvf $2.tar  $2       ;;
			bz2)	bzip $2 ;;
            gz)		gzip -c -9 -n $2 > $2.gz ;;
			zip)   	zip -r $2.zip $2   ;;
            7z)    	7z a $2.7z $2    ;;
            *)     	echo "'$1' cannot be packed via pk()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
alias pk-zip="pk zip $1"

# hcloud
ssh-hc () {
    if [ $2 ] ; then
        ssh $2@$(hcloud server ip $1)
    else
        ssh $(hcloud server ip $1)
    fi
}


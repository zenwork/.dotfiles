#!/usr/bin/env bash

yes | apt update -y && apt upgrade -y

yes | apt install \
    git \
    wget \
    curl \
    zsh \
    # fzf \
    vim-nox \
    vim-gtk \
    ctags \
    tmux \
    htop \
    tree \
    # grc \
    silversearcher-ag \
    build-essential \
    cmake \
    python-dev \
    python3-dev -y

rm -rf ~/.fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# https://github.com/soimort/translate-shell
wget git.io/trans -O ~/.dotfiles/bin/trans
chmod +x ~/.dotfiles/bin/trans

# ZSH
# chsh -s $(which zsh) $USER
usermod -s $(which zsh) $USER

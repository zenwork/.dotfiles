#!/usr/bin/env bash

currentDate=`date +"%Y-%m-%d_%T"`

function dotfiles_install {
    # $1 - file
    # $2 - source folder
    # $3 - dest dir
    if [ -e ~/$3$1 ]; then
        mkdir -p ~/tmp/.dotfiles_$currentDate
        mv ~/$3$1 ~/tmp/.dotfiles_$currentDate/
    fi
    ln -s ~/.dotfiles/$2$1 ~/$3$1
}

# # Zathura
# mkdir -p ~/.config/zathura
# dotfiles_install zathurarc other/ .config/zathura/
#
# # GIT
# dotfiles_install .gitconfig git/
#
#
# # ZSH
# rm -rf ~/.oh-my-zsh
# git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
# dotfiles_install .zshrc zsh/
#
#
# # TMUX
# dotfiles_install .tmux.conf tmux/
#
#
# # Python PDB
# dotfiles_install .pdbrc.py pdb/
# dotfiles_install .pdbrc pdb/


# VIM
dotfiles_install .vimrc vim/
dotfiles_install .vim vim/
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim -c "PlugInstall"
